QT += quick qml charts

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    src/heatmapimageprovider.cpp \
    src/main.cpp \

HEADERS += \
    src/heatmapimageprovider.h \

RESOURCES += \
    src/qml.qrc \
    resources/resources.qrc \

include("src/third-party/QHeatMap/QHeatMap.pri")

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

OTHER_FILES += \
    README.md \

#CONFIG += qmltypes
#QML_IMPORT_NAME = io.qt.examples.backend
#QML_IMPORT_MAJOR_VERSION = 1
