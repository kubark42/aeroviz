#message($$_PRO_FILE_PWD_/lib/heatmapper.cpp)

SOURCES += \
    src/third-party/QHeatMap/lib/heatmapper.cpp \
    src/third-party/QHeatMap/lib/gradientpalette.cpp \

HEADERS += \
    src/third-party/QHeatMap/include/global.h \
    src/third-party/QHeatMap/include/gradientpalette.h \
    src/third-party/QHeatMap/include/heatmapper.h \
