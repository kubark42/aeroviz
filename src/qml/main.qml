import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15

//import BackEnd 1.0
//import HeatMapper 1.0

Item {
    width: batteryDisplay.width
    height: batteryDisplay.height

    property int numSeriesCells: 12
    property int numParallelCells: 4

    property int heatMapRadius: 75
    property int heatMapOpacity: 128

    Component {
        id: contactDelegate
        Rectangle {
            width: grid.cellWidth; height: grid.cellHeight

            Image {
                height: grid.cellWidth * .75

                fillMode: Image.PreserveAspectFit

                source: imagePath
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
            }
        }
    }

    Rectangle {
        id: batteryDisplay
        width: grid.cellWidth * 12
        height: grid.cellHeight * (contactModel.count/12)

        GridView {
            id: grid

            z: 0

            anchors.fill: parent
            cellWidth: 60
            cellHeight: cellWidth

            model: contactModel
            delegate: contactDelegate
        }

        Image {
            id: batteryHeatMap

            z:1

            anchors.fill: parent

            sourceSize.width: width
            sourceSize.height: height

        }

    }

    ListModel {
        id: contactModel

        Component.onCompleted: {
            for (let i=0; i<numSeriesCells * numParallelCells; i++) {
                append({"imagePath": "qrc:/icons/battery-empty-solid.svg"})
            }

            refreshHeatMap()
        }
    }

    /*
     * Refreshes the heat map. Uses a silly random number to ensure the new background isn't pulled from the cache
     */
    function refreshHeatMap() {
        batteryHeatMap.source = 'image://heat_map/{"numSeriesCells": ' + numSeriesCells +
                ", \"numParallelCells\": " + numParallelCells +
                ", \"radius\": " + heatMapRadius +
                ", \"opacity\": " + heatMapOpacity + "}/" + Math.random()

    }

    Button {
        x: 50
        y: 50
        onClicked: {
            refreshHeatMap()
        }

        text: "Reload"
    }
}
